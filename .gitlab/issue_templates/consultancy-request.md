> Title: [GITLAB-FEATURE adoption] Developer Consulting - CUSTOMERNAME

## Context

CUSTOMER wants to adopt GITLAB-FEATURE. The development and DevOps teams require training and real-life use cases to learn, adopt and measure their improved workflows.

### Consultancy details

- Customer:   (URL to SF)
- GitLab feature area:   (docs URL)
- Consultancy type:  (workshop, troubleshooting, AMA, etc.)

### Status

<!-- Briefly describe the current customer environment, status of the adoption, and all important versions, setup details that are necessary. -->

Environment:

- [ ] GitLab Self-managed, version: __
- [ ] GitLab.com, version: __
- [ ] IDEs: (version: __, GitLab plugin version: __)

Adoption status:

### Goals

<!-- Example for GitLab Duo. Please edit/update for the specific request area.-->
<!--
1. Provide consultancy / AMA on hands-on use cases for GitLab Duo
2. Work with customer engineering teams to learn and practice
3. Train and advise CSM/SA teams to share the knowledge
4. Unify the asks from the customer into "generic" content that works for all customer scenarios, and becomes a SSoT for field enablement.
--> 

### DRIs

<!-- Depending on the consulting team, fill in the DRIs and tag them. -->

- DevRel:
- CSM: 
- SA: 
- AE: 

## Resources

<!-- Link relevant product epics, customer account issues, feedback issues, documentation, etc. -->
1.  

## Results 

<!-- Document the results of the consultancy engagement. Troubleshooting help, created content, inspired feature proposals or bug reports. -->
1.  

### Influenced ARR

<!-- Document the impact of consultancy on business results. -->

## Tasks

<!-- Define the task list, from review and planning to execution for results for customers. -->

- [ ] Review and define requirements, tasks and collaboration
- [ ] Sync and CSM/SA support
- [ ] Troubleshooting support
- [ ] Workshops

### Ideas

<!-- Document ideas -->
- [ ] 

<!-- Keep the labels -->
/label ~developer-advocacy 

<!-- Optional: Adjust the Consulting team, defaults to Sales. -->
/label ~"DA-Type::Consulting" ~"DA-Status::ToDo" ~"DA-Consulting::Sales" 

<!-- Customer consultancy requests need to remain confidential. -->
/confidential 

<!-- Adjust assignees -->
/assign @dnsmichi 

<!-- Please add a due date (for event speaking requests, content delivery, etc.) - e.g. /due 2025-07-31 -->
/due 

<!-- Add adoption epic (modify/remove if needed) -->
<!-- Example: [GitLab Duo/AI/ML Adoption] Customer Consultancy - FY26 (DevRel) -->

/epic https://gitlab.com/groups/gitlab-com/marketing/developer-relations/-/epics/518


