<!-- Please follow the Code of Conduct Enforcement workflow documented in https://about.gitlab.com/handbook/marketing/community-relations/workflows-tools/code-of-conduct-enforcement/  --> 

## Incident 

<!-- Describe the situation and how it violates the GitLab Code of Conduct https://about.gitlab.com/community/contribute/code-of-conduct/ 

Please add details to helpful context to understand the situation. --> 


## Suggested actions 

<!-- Describe the suggested actions, and tag a community relations team member to review and approve. 
Escalate in Slack in the #abuse / #developer-evangelism channel by linking to this issue, when necessary.
--> 

## Resources 

<!-- Epic/Issue/MR URLs, external content URLs, etc. -->

### Screenshots 

<!-- Document the situation, add dates and timestamps.G--> 



<!-- Keep assignees. --> 
/assign @sugaroverflow 

/cc @gitlab-da 

<!-- Keep confidential. --> 
/confidential 

<!-- Keep the labels. -->
/label ~"DA-Type::Response" ~"DA-Status::Doing" 

<!-- Set a due date. -->
/due 1 week 
