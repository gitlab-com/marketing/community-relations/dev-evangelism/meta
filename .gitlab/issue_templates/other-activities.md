## Activity Summary

**Please consider addressing these key questions in your summary:**

1. Who is our audience?
    - A:

2. What is the best content type (blog post, video, how-to) to deliver this message?
    - A:

3. How does this content help our audience?
    - A:

4. How does this content help us achieve our goals?
    - A:

## Tasks

- [ ] 

## Relevant Issues, Epics or resources

-



<!-- these labels should be included on all templates -->
/label ~"developer-advocacy" 

<!-- Note: These are samples for guidance, please add relevant labels for activity region, type, quarter or any other labels relevant to your team/program. Please update or include additional relevant labels here. -->

/label ~"DA-Type::Process" ~"DA-Status::ToDo"



<!-- Mention team members that should be aware of the epic -->
/cc