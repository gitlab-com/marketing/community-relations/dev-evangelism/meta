# Fix Friday - Highspot and Website Content Review

<!-- Suggested title format: "Fix Friday: Highspot and Website Content Review - Month Year" -->

## Summary

In an effort to ensure content items are up-to-date and technically accurate, the Developer Relations team will review the top performing assets on Highspot and about.gitlab.com. Please ensure the following: 
1. Content on the page is technically accurate.
1. Content is aligned with GitLab's current messaging.
1. Links are working and directing people to the best available page.

If you see that corrections or updates are needed, please follow [the process detailed in the handbook](https://about.gitlab.com/handbook/marketing/developer-relations/workflows-tools/content-review/#providing-feedback). 

## For review 

The following assets will be reviewed this month: 

### Highspot 

<!-- Include top 10 most viewed pages on Highspot that have not been reviewed in the past 2 Fix Friday cycles -->
- [ ]  
	
### about.gitlab.com 

<!-- Include top 10 most viewed pages from about.gitlab.com that have not been reviewed in the past 2 Fix Friday cycles --> 
- [ ]  

/label ~"DA-Requester-Type::Internal" ~"DA-Status::ToDo" ~"DA-Type::Content" ~"DevRel-Content" ~"Department::Developer Relations" 

/cc @gitlab-da
