# Developer Relations Content Request 

> Suggested title format: "Content type: Title - Author - Publish Month, Publish Year"  
> Dates should use ISO dates, see https://about.gitlab.com/handbook/communication/#writing-style-guidelines  

## Activity Summary
**Please provide a brief overview of the request.**

## Request details
**Please answer the following questions to help understand your request and its priority.**

1. What is the business goal?  
    - A: 

2. Who is the target persona/audience? What is the size of the audience? 
    - A: 

3. What is the best content type (blog post, video, how-to) to deliver this message?
    - A: 

4. How will this content be distributed? 
    - A: 

5. How does this content help our audience?
    - A: 

6. How does this content help us achieve our goals?
    - A: 

## Team or Individual DRIs

> Note: to be assigned by the Developer Relations team. DevRel team members: Please add appropriate team label upon assignment. 

## Tasks

- 

## Results

- 

## Relevant Issues, Epics or resources


---

<!-- these labels should be included on all templates -->
/label ~"Department::Developer Relations" ~"developer-advocacy" 

<!-- Note: These are samples for guidance, please add relevant labels for activity region, type, quarter or any other labels relevant to your team/program. Please update or include additional relevant labels here. -->

/label ~"DA-Type::Content" ~"FY26-Q1" ~"DA-Status::ToDo"

<!-- Content Type, we will use blog as the default, you can see other types in https://gitlab.com/groups/gitlab-com/-/labels?subscribed=&sort=relevance&search=DA-Content-Type -->

/label ~"DA-Type-Content::blog" 

<!-- Please add a due date (for event speaking requests, content delivery, etc.) - e.g. /due 2025-07-31 -->
/due 

<!-- Mention team members that should be aware of the content request. -->
/cc @gitlab-da 
