## :eyes: Pins

* [Release Evangelism handbook](https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/social-media/#release-evangelism) provides all resources.
* [#release-post](https://gitlab.slack.com/archives/C3TRESYPJ) Slack channel (internal)
* [Release post MR items](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?label_name%5B%5D=release+post)

### :construction_site: Tasks

1. Create campaign short URLs in the [Release Evagelism Campaign](https://campaign-manager.gitlab.com/campaigns/view/3), under the Developer Relations Team in [CommunityApps](https://campaign-manager.gitlab.com/), and add them into the releases sections below.
1. Review and identify social share items, add text examples for team members to copy/paste.

## :bulb: Releases

- https://about.gitlab.com/releases/
- https://about.gitlab.com/upcoming-releases/

<!-- Repeat the template below 12x for all releases for the coming fiscal year. -->
### <version> (yyyy-mm-dd)

- Release blog post:
- Twitter/X:
- LinkedIn:
- Mastodon:

Social shares

1.

Feature highlights

| Feature | Section | URL | Social text example |
|---------|---------|-----|---------------------|
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |


/label ~"developer-advocacy" ~"DA-Type::Content" ~"DevRel-Content" ~"Content-Releases"
