<!-- Hey, Thank you for taking the time to create a request for the Developer Advocacy team. Please review the following sections, and add as many details as possible. Helpful resources: https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/ -->

<!-- Please use the format below to add an Issue title. Dates should be using ISO dates, see https://about.gitlab.com/handbook/communication/#writing-style-guidelines -->

<!-- DA Request: <request type, short description> - Due: <Due Date (optional)> -->

# Request Info

### :page_with_curl: Request 

> Update the requestor and due date. Event information is optional. 

| Requestor | Due date     | 
|-----------|--------------|
| `@you`    | `2023-MM-DD` |

If it is an event, please provide the following details, remove this section otherwise:

| Event name | URL | Location | Dates |
|------|------|------|------|
|  |     |   | `2023-MM-DD` | 

### :page_with_curl: Request Type

> Requests from other team members at GitLab.

- [ ] [Content][content]
    - [ ] Creation (blog, demo, webinar, talks, by-lines, etc.)
    - [ ] Review request (blog, talks, docs, FAQs, etc.)
- [ ] [Speaking][speaking]
    - [ ] Speaking engagement / event coverage (in-person/virtual)
    - [ ] Media coverage (interview, podcast, etc.)
- [ ] [Speaker Enablement][speaker-enablement]
    - [ ] CFP submission review
    - [ ] Speaker coaching

For community response requests, please follow [this workflow][community-response].

### :speaking_head: Request Details 

> Add as many details as possible about the request.

1. 


### :building_construction: Request Triage Information

> This information will help the Developer Advocacy team triage the request and understand its impact when weighed against other requests in flight.

* Audience Description: `Who is this for?  What's the intended audience?`
* Expected Audience Size (live): `500`
* Expected Audience Size (post-creation online audience): `1200`
* Is this a new/nascent audience for GitLab (e.g. .NET developers)? `Yes/No`
* If this is a speaking session, will it be recorded and available for promotion? `Yes/No`

### :exclamation: Non-DevAdvocacy Team Request Checklist (IMPORTANT)

This section is important if you from a different team at GitLab. It will help us triage your issue appropriately. > To help us track our [DA Budgets][DA-budgets] please assign the right label that identifies the requesting team and a weight correlating with this issue's [budget score][DA-scoring-requests] to allow us to track each team's budget consumption.


- [ ] `DA-Issue-Type::External`  assigned
- [ ] `DA-Type::Consulting` assigned
- [ ] Due Date added to issue.
- [ ] Selected the right option from the `DA-Consulting` scoped label options that represent the department the request belongs to
- [ ] Assigned the right Issue weight based on the type of request and your team/org, [learn more here][DA-scoring-requests]
- [ ] A DRI has been assigned from the Developer Advocacy Team or issue has been assigned to `@johncoghlan` where no DRI is known yet.
- [ ] If the need is within the next week, please also slack the team in the [`#dev-advocacy-team`][dev-advocacy-team-slack] channel.


# Post Request Completion

## Request Completion Details

* Date of Execution/Completion: `<Date>`
* Link to resource: `<Video, Link, Doc, etc.>`
* Other Relevant Details: 

## Metrics

*Please provide any available metrics or accessible link to metrics*

## CheckLists

### Developer Advocacy 

* [ ] Added to [team calendar](team-calendar)



FYI: @gitlab-da

<!-- Please keep the assignees, optional: change to the Developer Advocacy DRI -->
/assign @johncoghlan 

<!-- Requestor: Please leave the labels below on this issue -->
/label ~"developer-advocacy" ~"DA-Status::ToDo" ~"DA-Requester-Type::External" ~"DA-Type::Consulting"

<!-- Developer Advocates: Uncomment this block to apply internal labels. -->
<!--
/label ~"developer-advocacy" ~"DA-Status::ToDo" ~"DA-Requester-Type::Internal" ~"DA-Type::Content" 
-->


<!-- Please add a due date (for event speaking requests, content delivery, etc.) - e.g. /due 2025-07-31 -->
/due 

<!-- Please ensure that the shared data is SAFE, and make this issue confidential otherwise. https://about.gitlab.com/handbook/legal/safe-framework/ -->

<!-- 
/confidential 
--> 

---

<!-- DO NOT REMOVE -->
[content]: https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/#content-creation
[speaking]: https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/#corporate-event-support
[speaker-enablement]: https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/speaker-enablement/
[team-calendar]: https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/calendar/
[community-response]: https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/community-response/
[DA-budgets]: https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/workflow/#request-budgets
[DA-scoring-requests]: https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/workflow/#scoring-requests
[dev-advocacy-team-slack]: https://app.slack.com/client/T02592416/CMELFQS4B
