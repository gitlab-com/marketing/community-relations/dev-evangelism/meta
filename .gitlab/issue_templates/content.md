> Issue Title Guide:
> Dates should be using ISO dates, see https://about.gitlab.com/handbook/communication/#writing-style-guidelines
> Format:
> Content: Title - Author - Publish Month, Publish Year


## Activity Summary
**Please provide a brief overview of the content activity.**


### Details
**Please consider addressing these key questions in your summary:**

1. Who is our audience?
    - A:

2. What is the best content type (blog post, video, how-to) to deliver this message?
    - A:

3. How does this content help our audience?
    - A:

4. How does this content help us achieve our goals?
    - A:

## Tasks

- 

## Relevant Issues, Epics or resources

-

## Results

- 

## Checklist
**Please review the tasks and tick them off.**

On completion:

- [ ] [Distribute the content](https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/content/#content-distribution) (Highspot, Slack, social media, etc.)
- [ ] Add results to this issue.

<!-- these labels should be included on all templates -->
/label ~"developer-advocacy" 

<!-- Note: These are samples for guidance, please add relevant labels for activity region, type, quarter or any other labels relevant to your team/program. Please update or include additional relevant labels here. -->

/label ~"DA-Type::Content" ~"FY26-Q1" ~"DA-Status::ToDo" ~"DA-Content::New"

<!-- Content Type, we will use blog as the default, you can see other types in https://gitlab.com/groups/gitlab-com/-/labels?subscribed=&sort=relevance&search=DA-Content-Type -->

/label ~"DA-Type-Content::blog"

<!-- Please add a due date (for event speaking requests, content delivery, etc.) - e.g. /due 2025-07-31 -->
/due 

<!-- FY26 -->
/epic https://gitlab.com/groups/gitlab-com/marketing/developer-relations/-/epics/521 

<!-- Assign yourself -->
/assign me

<!-- Mention team members that should be aware of the epic -->
/cc